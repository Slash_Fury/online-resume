<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/common"%>
<%@ taglib prefix="content" tagdir="/WEB-INF/tags/content"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<common:common>
    <jsp:body>
        <content:hello />
    </jsp:body>
</common:common>
