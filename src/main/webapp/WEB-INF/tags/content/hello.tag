<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container-fluid">
    <c:choose>
        <c:when test="${not empty name}">
            <p>Wassup, ${name}?!</p>
        </c:when>
        <c:otherwise>
            <p>Wassup?!</p>
        </c:otherwise>
    </c:choose>
</div>